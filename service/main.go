package service

// NODE is node name to be used as runtime
const NODE = "YourNodeName"

// Execute is only function to be called from MicroEngine
// You can use serviceName to saperate your function for example below
func (c *ServicePlugin) Execute(serviceName string, input interface{}) (interface{}, error) {

	// Stamp call execute
	c.Logger.Info().Msg(c.TraceId, "Start Execute")
	defer c.Logger.Info().Msg(c.TraceId, "End Execute")

	// Set input data to map
	// If input has data inside, set inMap = data
	inMap := input.(map[string]interface{})
	if inMap["data"] != nil {
		inMap = inMap["data"].(map[string]interface{})
	}

	// Make variable to get result from controller
	var err error
	var result interface{}

	if serviceName == "yourFuncA" {

		// Call controller for your function YourFuncA
		//result, err = c.ServiceController.YourFuncA(serviceName, inMap)

	} else if serviceName == "yourFuncB" {

		// Call controller for your function YourFuncB
		//result, err = c.ServiceController.YourFuncB(serviceName, inMap)

	}

	// Convert result to output data
	output := c.Context.Reply(err, NODE, serviceName, result, 0)

	// Stamp output data
	c.Logger.Debug().Msgf(c.TraceId, "output=%v", output)

	return output, nil

}
