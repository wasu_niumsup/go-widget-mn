package accessors

import (
	"fmt"
	"techberry-go/common/v2/facade"
)

// Make variable for accessors package
var DBName string = "db"
var CollName string = "coll"

// This is example function to call Restheart
// ListData is function to list data in the collection of mongoDB
func (c *ServiceAccessor) ListData(pageIndex int, pageSize int) (interface{}, error) {

	// Stamp Log
	c.Logger.Info().Msg(c.TraceId, "Start Accessor ListData")
	defer c.Logger.Info().Msg(c.TraceId, "End Accessor ListData")

    // Call Restheart by default server name
	res, err := c.ServiceNode.Restheart("").
		Database(DBName).
		Collection(CollName).
		Method(facade.RH_GET_METHOD).
		Page(pageIndex, pageSize).
		Do()

	return res, err
}

// This is example function to call DataFactory, you can call database using DataFactory
// SearchDBData is function to find record in database
func (c *ServiceAccessor) SearchDBData(params map[string]interface{}) (interface{}, error) {

	// Stamp Log
	c.Logger.Info().Msg(c.TraceId, "Start Accessor SearchDBData")
	defer c.Logger.Info().Msg(c.TraceId, "End Accessor SearchDBData")

	res, err := c.ServiceNode.DataFactory("").SelectMany("ssquare", "find_option", params)

	return res, err
}